<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $status = Array('Aktif','Blokir','Tutup');
        $jenis = Array('Tabungan Sekarang','Tabungan Investasi','Tabungan Masa Depan');

            for($i=1;$i<=100;$i++){

            // $this->call(UsersTableSeeder::class);
            DB::table('costumer')->insert([
                'no_rek' => $faker->numberBetween(100,100000),
                'nama_cs' => $faker->name,
                'nik_cs' => $faker->numberBetween(10000000,50000000),
                'alamat_cs' => $faker->address,
                'status' => $status[array_rand($status)],
                'jenis' => $jenis[array_rand($jenis)],
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
            ]);
            
        }
    }
}
