<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class indexModels extends Model
{
    //
    protected $table = "tbl_siswa";
    protected $fillable = [
    'nama_siswa','jurusan_siswa','email_siswa','alamat_siswa'
];
}
