<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    //
    protected $table = 'transaksi';
    protected $fillable = [
        'no_rek','nama_cs','nik','alamat_cs','status','jenis'
    ];
}
