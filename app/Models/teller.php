<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class teller extends Model
{
    //
    protected $table = 'costumer';
    protected $primaryKey = 'no_rek';

    public $incrementing = false;

    protected $fillable = [
        'no_rek','nama_cs','nik','alamat_cs','status','jenis'
    ];

}
