<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\teller;
use Illuminate\Support\Facades\DB;

class indexController extends Controller
{
    public function teller(){
        $data = teller::all();
       

        return view('teller',['teller' => $data]);
    }

    public function cari(Request $r){
        $cari = $r->cari;

        $data = DB::table('costumer')
        ->where('no_rek','like','%'.$cari.'%')
        ->paginate();

        return view('teller',['teller' => $data]);
    }

    

    public function simpanCostumer(Request $r){
        $no_rek = $r->input('no_rek');
        $nama_cs = $r->input('nama_cs');
        $nik_cs = $r->input('nik');
        $alamat_cs = $r->input('alamat_cs');
        $jenis = $r->input('jenis');

        $insert = new teller();
        $insert->no_rek = $no_rek;
        $insert->nama_cs = $nama_cs;
        $insert->nik_cs = $nik_cs;
        $insert->status = 'Tutup';
        $insert->alamat_cs = $alamat_cs;
        $insert->jenis = $jenis;
        $insert->save();

        \DB::table('saldo')->insert([
            'no_rek' => $r->no_rek,
            'saldo' => '0'
        ]);
        

        return redirect('/teller/#tabs-3');
    }
    
        public function edit(Request $r){
        DB::table('costumer')->where('no_rek',$r->norek)->update([
            'status' => $r->status
        ]);

        return redirect('/teller');
    }
    
    public function hapus($id){
        $data = teller::find($id);
        $data->delete();
        
        return redirect('/teller/#tabs-3');
    }

    
}
