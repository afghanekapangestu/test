<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\teller;

class transaksiController extends Controller
{
    //
    

    public function getAkun($no_rek){
        $data = \DB::select("SELECT costumer.nama_cs, saldo.saldo from costumer JOIN saldo ON costumer.no_rek = saldo.no_rek WHERE costumer.no_rek='$no_rek'");
        return response()->json($data);
    }

    public function setoranPost(Request $request){
        \DB::table('transaksi')->insert([
            'Keterangan' => 'Setoran',
            'Nominal' => $request->nominal,
            'saldo_awal' => $request->saldo_awal,
            'no_rek' => $request->no_rek
        ]);

        \DB::table('saldo')->where('no_rek',$request->no_rek)->update([
		    'saldo' => $request->nominal+$request->saldo_awal,
	    ]);

        return redirect()->back();
    }

    public function tarikTunai(Request $request){
        \DB::table('transaksi')->insert([
            'Keterangan' => 'Tarik Tunai',
            'Nominal' => $request->nominal,
            'saldo_awal' => $request->saldo_awal,
            'no_rek' => $request->no_rek
        ]);

        \DB::table('saldo')->where('no_rek',$request->no_rek)->update([
		    'saldo' =>$request->saldo_awal - $request->nominal,
	    ]);

        return redirect()->back();
    }

    public function transferSaldo(Request $request){
        
        $no_rekd = $request->no_rek;
        $saldobaru = $request->saldo;
        $no_rek = $request->no_rek1;
        \DB::select( \DB::raw("UPDATE saldo SET saldo=saldo.saldo-:saldobaru  WHERE no_rek = :no_rekd"), array('saldobaru' => $saldobaru, 'no_rekd' => $no_rekd));
        
        
        \DB::select( \DB::raw("UPDATE saldo SET saldo=saldo.saldo+:saldobaru  WHERE no_rek = :no_rek"), array('saldobaru' => $saldobaru, 'no_rek' => $no_rek));

        \DB::table('transaksi')->insert([
            'Keterangan' => 'Transfer',
            'Nominal' => $request->saldo,
            'saldo_awal' => $request->saldo_awal,
            'no_rek' => $request->no_rek
        ]);


        return redirect()->back();
    }

    public function selectDataPeriode($no_rek,$nama,$periode,$periode2){
        $data = \DB::select(\DB::raw("SELECT transaksi.no_rek,transaksi.nominal,transaksi.saldo_awal,transaksi.tanggal,transaksi.keterangan FROM transaksi JOIN costumer ON transaksi.no_rek = costumer.no_rek WHERE transaksi.no_rek = '$no_rek' AND costumer.nama_cs ='$nama' LIMIT $periode,$periode2"));

        return view('table',['table' => $data]);
    }
}
