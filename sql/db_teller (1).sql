-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2019 at 04:42 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_teller`
--

-- --------------------------------------------------------

--
-- Table structure for table `costumer`
--

CREATE TABLE `costumer` (
  `no_rek` int(8) NOT NULL,
  `nama_cs` varchar(255) NOT NULL,
  `nik_cs` int(11) NOT NULL,
  `alamat_cs` text NOT NULL,
  `status` enum('Aktif','Blokir','Tutup','') NOT NULL,
  `jenis` enum('Tabungan Investasi','Tabungan Sekarang','Tabungan Masa Depan','') NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costumer`
--

INSERT INTO `costumer` (`no_rek`, `nama_cs`, `nik_cs`, `alamat_cs`, `status`, `jenis`, `created_at`, `updated_at`) VALUES
(123, 'mama', 123, '123', 'Tutup', 'Tabungan Investasi', '2019-08-28', '2019-08-28'),
(1234567890, 'Afghan Eka Pangestu', 12345678, 'Perumahan', 'Aktif', 'Tabungan Masa Depan', '2019-08-28', '2019-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `saldo`
--

CREATE TABLE `saldo` (
  `no_rek` int(8) NOT NULL,
  `saldo` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saldo`
--

INSERT INTO `saldo` (`no_rek`, `saldo`) VALUES
(123, 2000),
(1234567890, 100005400);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `keterangan` text NOT NULL,
  `nominal` int(255) NOT NULL,
  `saldo_awal` int(255) NOT NULL,
  `no_rek` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`tanggal`, `keterangan`, `nominal`, `saldo_awal`, `no_rek`) VALUES
('2019-08-28 17:25:55', 'Setoran', 150000, 0, 1234567890),
('2019-08-28 18:31:00', 'Transfer', 1000, 6400, 1234567890),
('2019-09-01 14:23:34', 'Setoran', 100000000, 5400, 1234567890);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`no_rek`);

--
-- Indexes for table `saldo`
--
ALTER TABLE `saldo`
  ADD UNIQUE KEY `no_rek` (`no_rek`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD KEY `no_rek` (`no_rek`) USING BTREE;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `saldo`
--
ALTER TABLE `saldo`
  ADD CONSTRAINT `saldo_ibfk_1` FOREIGN KEY (`no_rek`) REFERENCES `costumer` (`no_rek`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`no_rek`) REFERENCES `costumer` (`no_rek`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
