<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Halaman</title>

    {{-- Boostrap  --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/datatables.min.js')}}"></script>
    <script src="{{asset('js/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/accounting.min.js')}}"></script>
   

    <style>
      #tbody{
        cursor: pointer;
      }
      
      #tableData tr td{
        padding-top: 2%;
      }

      

    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=".">Teller Laravel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="teller">Teller Menu</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="transaksi">Transaksi</a>
            </li>
           
      </nav>

      <div class="container mt-3">
        @if (Request::path() == '/')
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <h1 class="display-4">Selamat Datang</h1>
              <p class="lead">Silakan untuk menggunakannya, pergi ke halaman teller menu dan transaksi jika ingin melakukan aksi.</p>
            </div>
          </div>

        @elseif(Request::path() == 'teller')
          @yield('tellermenu')
        
        @elseif(Request::path() == 'transaksi')
          @yield('transaksi')
        @endif
        
        
        
      </div>
      <script>

          $(document).ready( function () {
            $( "#tabs,#tab" ).tabs();            
            $('#table2').DataTable({
              "pageLength" : 5,
              "dom" : "Bfrtip",
              "button" : [
                'copy','csv','print','pdf','excel'
              ]

              
            });
            
            
                
                
                
                
                

               
                });

            

           

            function functionCari(cari,no_rek,isi,isi2){
                  $(cari).click(function(){
                
                  var norek = $(no_rek).val();
                $.ajax({
                  url : '{{URL::to('/transaksi/display/')}}'+'/'+norek,
                  method : 'GET',
                  success: function(data){
                    $(isi).val(data[0]["saldo"]);
                    $(isi2).val(data[0]["nama_cs"]);

                  },
                  error: function(xhr, ajaxOptions, thrownEror){
                    alert('error'.thrownEror);
                  }
                  
                });
                
                
              });
            }

            functionCari('#cari0','#no_rek0','#isi0','#isi00');
            functionCari('#cari1','#no_rek1','#isi1','#isi11');
            functionCari('#cari2','#no_rek2','#isi2','#isi22');

            $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     }     

            });

            
            
            $("#buttonCek").click(function(){
              var no_rek = $("#cekNoRek").val();
              var nama = $('#cekNama').val();
              var periode1 = $('#cekPeriode').val()-1;
              var periode2 = $('#cekPeriode2').val();
              
              $("#tablelain tbody tr").empty();
              
              var currentrequest = null;
              currentrequest = jQuery.ajax({
                type : "POST",
                url : '{{URL::to('/transaksi/display1/')}}'+'/'+no_rek+'/'+nama+'/'+periode1+'/'+periode2,
                beforeSend : function(){
                  if(currentrequest != null){
                    currentrequest.abort();
                  }
                },

                success:function(data){
                  
                  
                  var mantul = JSON.parse(data);
                  $("#CariSemua").show();
                  $("#CariSemua").keyup(function(){
                    search_table($(this).val());
                    
                  });

                  function search_table(value){
                    $('#tablelain tbody tr').each(function(){
                      var found = 'false';
                      $(this).each(function(){
                        if($(this).text().toLowerCase().indexOf(value.toLowerCase()) >=0){
                          found = 'true';
                        }
                      });
                      if(found == 'true'){
                        $(this).show();
                      }else{
                        $(this).hide();
                      }
                    });
                  }

                  setTimeout(() => {
                      $.each(mantul,function(i,item){
                        $("#isitable").append("\n <tr align='center'><td>"+mantul[i].tanggal+"</td><td>"+mantul[i].keterangan+"</td><td>"+mantul[i].nominal+"</td><td>"+mantul[i].saldo_awal+"</td><tr>");
                        // myTable.row.add([
                        //   mantul[i].tanggal,
                        //   mantul[i].keterangan,
                        //   mantul[i].nominal,
                        //   mantul[i].saldo_awal
                          
                        //   ]);
                        // myTable.draw();
                        
                      });
                  }, 100);
                  

                  
                  
                },

                error:function(data){
                  alert('fail');
                }

              });

              

            });

            // function DataTable(){
            //   var no_rek = $("#cekNoRek").val();
            //   var nama = $('#cekNama').val();
            //   var periode1 = $('#cekPeriode').val();
            //   var periode2 = $('#cekPeriode2').val();
            //   var data = null;
              
              
            //   var table = $("#tablelain").DataTable({
            //        ajax : {
            //         url : '{{URL::to('/transaksi/display1/')}}'+'/'+no_rek+'/'+nama+'/'+periode1+'/'+periode2,
            //         dataSrc : '',
            //         beforeSend : function(){

            //         }
            //       },
            //       columns : [
            //         { "data" : 'no_rek'},
            //         { "data" : 'saldo_awal'},
            //         { "data" : 'nominal'},
            //         { "data" : 'nominal'}
                    
            //       ]
            //     });

            //     if($(".even, .odd")){
            //       $(".table tbody").addClass("important");
            //       setTimeout(() => {
            //         $(".important").empt
            //       }, 3000);
            //     }else{
                  
            //     }
            // }
            

            
            


            
            
            

          
          $(".tab").click(function(){
            $("input[type=number],input[type=text]").val("");
            $("#isitable").empty();
            $("#CariSemua").hide();
            
            
          });

          

          

          

          

          
        </script>
      
</body>
</html>