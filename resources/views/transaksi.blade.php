@extends('welcome')

@section('transaksi')
    <div id="tab">
        <ul>
            <li ><a href="#tab-1" class="tab">Lihat Transaksi</a> </li>
            <li ><a href="#tab-2" class="tab">Transfer </a> </li>
            <li ><a href="#tab-3" class="tab">Tarik Tunai</a> </li>
            <li ><a href="#tab-4" class="tab">Setor</a> </li>
            
        </ul>
        
        <div id="tab-1">
            <table>
                    
                    <tr>
                        <td>No Rekening</td>
                        <td>
                            <input type="text" id="cekNoRek">
                        </td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>
                            <input type="text" id="cekNama">
                        </td>
                    </tr>
                    <tr>
                        <td>Periode</td>
                        <td>
                            <input type="text" id="cekPeriode">
                        </td>
                        <td>
                            s/d
                        </td>
                        <td>
                            <input type="text" id="cekPeriode2">
                        </td>
                        <td>
                            <button type="button" id="buttonCek">Cek</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cari
                        </td>
                        <td>
                            <input type="text" id="CariSemua" style="display:none;" placeholder="Cari Data ">
                        </td>
                    </tr>
                
            </table>
            <br>
            
            <table class="table table-hover" id="tablelain">
                <thead align="center">
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                        <th>Saldo Awal</th>
                </thead>
                
                
                <tbody id="isitable" align="center">
                   
                </tbody>
            </table>
        </div>
            

        <div id="tab-2">
            <table cellpadding="3" cellspacing="3">
                
                    <form action="{{URL::action('transaksiController@transferSaldo')}}" method="post">
                
                <tr>
                    <td>
                        Dari No Rekening
                    </td>
                    <td>
                        <input type="number" name="no_rek" id="no_rek0">
                        @csrf
                    </td>
                    <td>
                        <button type="button" id="cari0" >Cek</button>
                    </td>
                </tr>

            
                
            
                <tr>
                    <td>
                        Saldo Awal
                    </td>
                    <td>
                        <input type="text" name="saldo_awal" id="isi0" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        Atas Nama
                    </td>
                    <td>
                        <input type="text" id="isi00" readonly>
                    </td>
                </tr>
                <tr>
                    <td>No Rekening Tujuan</td>
                    <td>
                        <input type="number" name="no_rek1">
                    </td>
                </tr>
                <tr>
                    <td>
                        Jumlah Transfer
                    </td>
                    <td>
                        <input type="number" name="saldo">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Submit">
                    </td>
                </tr>
            
            </form>
            
                
            </table>
        </div>

        <div id="tab-3">
            <table>
                    <form action="{{URL::action('transaksiController@tarikTunai')}}" method="post">
                <tr>
                    <td>No Rek Tujuan</td>
                    <td>
                        <input type="number" name="no_rek" id="no_rek1">
                        @csrf
                    </td>
                    <td>
                        <button type="button" id="cari1">Cek</button>
                        
                    </td>
                </tr>
                <tr>
                    <td >
                        Saldo Awal
                    </td>
                    <td>
                        <input type="text" name="saldo_awal" id="isi1" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nominal
                    </td>
                    <td>
                        <input type="number" name="nominal">
                    </td>
                </tr>
                <tr>
                    <td>Atas Nama</td>
                    <td>
                        <input type="text" id="isi11" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                            <input type="submit" value="Submit">
                    </td>
                </tr>

                    </form>
            </table>
        </div>

        <div id="tab-4">
            <table>

                <form action="{{URL::action('transaksiController@setoranPost')}}" method="post">

                    <tr>
                        <td>No Rek Tujuan</td>
                        <td>
                            <input type="number" name="no_rek" id="no_rek2">
                            @csrf
                        </td>
                        <td>
                            <button type="button" id="cari2">Cek</button>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Saldo Awal
                        </td>
                        <td>
                                <input type="text" name="saldo_awal" id="isi2" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>Atas Nama</td>
                        <td>
                            <input type="text" id="isi22" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>Jumlah Setoran</td>
                        <td>
                            <input type="number" name="nominal">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="Submit">
                        </td>
                    </tr>

                 </form>
            </table>
        </div>
    </div>
@endsection