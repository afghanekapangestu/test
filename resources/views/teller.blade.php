    @extends('welcome')

    @section('tellermenu')
    
    <div id="tabs">
            <ul>
            <li><a href="#tabs-1">Lanjut</a></li>
            <li><a href="#tabs-2">Daftar</a></li>
            <li><a href="#tabs-3">Lihat</a></li>
            </ul>
            <div id="tabs-1">
            
    <form action="{{URL::action('indexController@edit')}}" method="POST">
            <table>
                <tr>
                    <td>No Rekening</td>
                    <td>
                        @csrf
                        <input type="number" name="norek" id="norek">
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <select name="status">
                            <option value="Aktif">Aktifkan No Rekening</option>
                            <option value="Blokir">Blokir No Rekening</option>
                            <option value="Tutup">Tutup No Rekening</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit"></td>
                </tr>
            </table>
        </form>
            </div>
            <div id="tabs-2">
                    <form action="{{URL::action('indexController@simpanCostumer')}}" method="POST" class="w-100 d-block">
                            <table  cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        Nama 
                                    </td>
                                    <td>
                                        {{ csrf_field() }}
                                        <input type="text" name="nama_cs" autocomplete="off" id="nama_cs" >
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        NIK 
                                    </td>
                                    <td>
                                        <input type="text" name="nik" autocomplete="off" id="nik">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        No Rekening 
                                    </td>
                                    <td>
                                        <input type="number" name="no_rek" autocomplete="off" id="no_rek">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Alamat 
                                    </td>
                                    <td>
                                        <textarea name="alamat_cs" autocomplete="off" id="alamat_cs" cols="50" rows="5" style="resize:none;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status 
                                    </td>
                                    <td>
                                        <select name="jenis" autocomplete="off" id="jenis">
                                            <option value="Tabungan Masa Depan">Tabungan Masa Depan</option>
                                            <option value="Tabungan Investasi">Tabungan Investasi</option>
                                            <option value="Tabungan Masa Kini">Tabungan Masa Kini</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="submit" value="Submit" class="btn btn-primary">
                                    </td>
                                </tr>
                            </table>
                        </form>
            </div>
            <div id="tabs-3">
            
   
        <div class="table mt-2">
            <table border="1" class="table table-bordered table-hover table-responsive w-100 d-block d-md-table" id="table2">
                <thead align="center">
                    <tr>
                            <th>No Rekening</th>
                            <th>Nama Costumer</th>
                            <th>NIK</th>
                            <th>Alamat Costumer</th>
                            <th>Status</th>
                            <th>Jenis</th>
                            <th>Aksi</th>
                    </tr>
                </thead>
                
                <tbody align="center">
                    @foreach ($teller as $item)
                        <tr>
                            
                            <td>{{$item->no_rek}}</td>
                            <td>{{$item->nama_cs}}</td>
                            <td>{{$item->nik_cs}}</td>
                            <td>{{$item->alamat_cs}}</td>
                            <td>{{$item->status}}</td>
                            <td>{{$item->jenis}}</td>
                            <td>
                                    <i class="fas fa-align-center"></i>
                                <a href="teller/{{$item->no_rek}}/hapus">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
            </div>
        </div>
    </div>

    @endsection


