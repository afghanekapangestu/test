<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/teller', 'indexController@teller');
Route::get('/teller/{id}/hapus', 'indexController@hapus');
Route::get('/transaksi', function(){
    return view('transaksi');
});
Route::get('/transaksi/display/{no_rek}/', 'transaksiController@getAkun');
Route::post('/transaksi/display1/{no_rek}/{nama}/{periode}/{periode2}', 'transaksiController@selectDataPeriode');

Route::post('/teller/simpanCostumer', 'indexController@simpanCostumer');
Route::post('/teller/update', 'indexController@edit');


Route::post('/transaksi/setoranPost', 'transaksiController@setoranPost');
Route::post('/transaksi/tarikTunai', 'transaksiController@tarikTunai');
Route::post('/transaksi/transferSaldo', 'transaksiController@transferSaldo');
    